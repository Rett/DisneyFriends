//
//  LocationAnnotationsView.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 12/26/19.
//  Copyright © 2019 Rettify. All rights reserved.
//

import UIKit
import MapKit

struct LocationCollection : Codable {
    static func LocationsPath() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Locations.geojson")
    }
    init() {
        do {
            let jsonData = try Data(contentsOf: LocationCollection.LocationsPath())
            let result = try JSONDecoder().decode(LocationCollection.self, from: jsonData)
            self.features = result.features;
            self.type = result.type;
        } catch {
            print("Error while parsing: \(error)")
            self.features = [];
            self.type = "NULL";
        }
    }
    
    // TODO: Moving annotations is possibly duplicating them
    mutating func Update(annotation: LocationAnnotation) {
        var found_feature = false;
        for i in self.features.indices {
            if (self.features[i].uuid == annotation.uuid.uuidString) {
                found_feature = true;
                self.features[i].type = LocationType.ToString(type: annotation.type);
                self.features[i].geometry = GeometryProperties(type: "Point", coordinates: [annotation.coordinate.latitude, annotation.coordinate.longitude]);
                self.features[i].properties = Properties(title: annotation.title!, description: annotation.subtitle!, done: annotation.done);
                break;
            }
        }
        
        if (!found_feature) {
            let geo_properties = GeometryProperties(type: "Point", coordinates: [annotation.coordinate.latitude, annotation.coordinate.longitude]);
            let properties = Properties(title: annotation.title!, description: annotation.subtitle!, done: annotation.done);
            let new_feature = Feature(uuid: UUID().uuidString, type: LocationType.ToString(type: annotation.type), geometry: geo_properties, properties: properties)
            self.features.append(new_feature);
        }
        
        do {
            let json = try JSONEncoder().encode(self);
            do {
                try json.write(to: LocationCollection.LocationsPath());
            } catch {
                print("Error while writing locations: \(error)")
            }
        } catch {
            print("Error while encoding locations: \(error)")
        }
    }
    
    mutating func Remove(annotation: LocationAnnotation) {
        self.features.removeAll(where: { $0.uuid == annotation.uuid.uuidString });
        
        do {
            let urlBar = Bundle.main.url(forResource: "Locations", withExtension: "geojson")!
            let json = try JSONEncoder().encode(self);
            do {
                try json.write(to: urlBar);
            } catch {
                print("Error while writing locations: \(error)")
            }
        } catch {
            print("Error while encoding locations: \(error)")
        }
    }
    
    var type : String
    var features : [Feature]
}

struct Feature : Codable {
    var uuid : String
    var type : String
    var geometry : GeometryProperties
    var properties: Properties
}

struct GeometryProperties : Codable {
    var type : String
    var coordinates : [Double]
}

struct Properties : Codable {
    var title : String
    var description : String
    var done : Bool
}

class LocationAnnotationsView: MKMarkerAnnotationView {
  // Required for MKAnnotationView
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func MaskRoundedImage(image: UIImage, radius: CGFloat) -> UIImage {
        let imageView: UIImageView = UIImageView(image: image);
        imageView.frame.size.width = 40;
        imageView.frame.size.height = 40;
        
        let layer = imageView.layer;
        layer.masksToBounds = true
        layer.cornerRadius = radius
        
        UIGraphicsBeginImageContext(imageView.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!
    }

    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.clusteringIdentifier = reuseIdentifier;
        
        let clusterAnnotation = self.annotation as? MKClusterAnnotation
        if (clusterAnnotation != nil) {
            self.glyphTintColor = .white;
            self.glyphText = String(clusterAnnotation!.memberAnnotations.count);
            self.markerTintColor = .systemBlue;
            return;
        }
        
        guard let locationAnnotation = self.annotation as? LocationAnnotation else { return }
        
        locationAnnotation.typeChanged = {
            self.glyphImage = locationAnnotation.done ? UIImage(systemName: "checkmark.circle") : locationAnnotation.type.Image();
        }
        locationAnnotation.doneChanged = {
            self.glyphImage = locationAnnotation.done ? UIImage(systemName: "checkmark.circle") : locationAnnotation.type.Image();
            self.markerTintColor = locationAnnotation.done ? .systemGreen : .systemBlue;
        }
        self.glyphImage = locationAnnotation.done ? UIImage(systemName: "checkmark.circle") : locationAnnotation.type.Image();
        self.glyphTintColor = .white;
        self.markerTintColor = locationAnnotation.done ? .systemGreen : .systemBlue;
  }
}
