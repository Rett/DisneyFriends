//
//  DisneyMapOverlay.swift
//  DisneyFriends
//
//  Created by Garrett on 8/12/18.
//  Copyright © 2018 Garrett. All rights reserved.
//

import Foundation
import MapKit


public struct DisneyTile {
    
    /// Path for the tile with `maximumZ` supported by the tile server set in the config.
    /// This is the path with the best resolution tile from wich this zoomable tile can be interpolated.
    /// - SeeAlso: `MapCacheConfig``
    let maximumZPath: MKTileOverlayPath
    
    /// Rectangle area ocupied by this tile
    let rect: MKMapRect
    
    /// Scale over the tile of the maximumZ path.
    /// It is a multiple of 2 (2, 4, 8).
    /// For a zoom larger than maximumZ represents the number of tiles the original tile is divided in
    /// one axis. For example,  overZoom=4 means that in each axis the tile is divided in 4 as well as
    /// in the Y axis. So, the original tile at maximumZ is divided in 16 subtiles.
    /// The `rect`tells us, among those tiles, which one is this tile.
    let overZoom: Int
}


class DisneyMapOverlay: MKTileOverlay
{
    override func url(forTilePath path: MKTileOverlayPath) -> URL
    {
        let tile_url = "https://cdn6.parksmedia.wdprapps.disney.com/media/maps/prod/141/\(path.z)/\(path.x)/\(path.y).jpg";
        return URL(string: tile_url)!
    }
    
    func TilesInMapRect(z: Int, rect: MKMapRect, scale: MKZoomScale) -> [DisneyTile] {
        var tiles: [DisneyTile] = []
        let tileSize = CGSize(width: z < 19 ? 256 : 512, height: z < 19 ? 256 : 512)
        print("Zoom Scale \(scale)  Zoom Level: \(z)")
       
        var overZoom = 1
        let tileSetMaxZ = 20
        var mod_z = z
        if (z > tileSetMaxZ) {
            overZoom = Int(pow(2.0, Double(z - tileSetMaxZ)))
            mod_z = tileSetMaxZ
        }
        
        let adjustedTileSize = Double(overZoom * Int(tileSize.width))
        
        let minX = Int(floor((rect.minX * Double(scale)) / adjustedTileSize))
        let maxX = Int(floor((rect.maxX * Double(scale)) / adjustedTileSize))
        let minY = Int(floor((rect.minY * Double(scale)) / adjustedTileSize))
        let maxY = Int(floor((rect.maxY * Double(scale)) / adjustedTileSize))
        
        for x in minX ... maxX {
            for y in minY ... maxY {
                
                let point = MKMapPoint(x: (Double(x) * adjustedTileSize) / Double(scale),
                                       y: (Double(y) * adjustedTileSize) / Double(scale))
                let size = MKMapSize(width: adjustedTileSize / Double(scale),
                                     height: adjustedTileSize / Double(scale))
                let tileRect = MKMapRect(origin: point, size: size)
                
                // check that a portion of the tile intersects with the maps rect
                // no need to do the work on a tile that won't be seen
                guard rect.intersects(tileRect) else { continue }
                
                let path =  MKTileOverlayPath(x: x, y: y, z: mod_z, contentScaleFactor: scale)
                let tile = DisneyTile(maximumZPath: path, rect: tileRect, overZoom: overZoom)
                tiles.append(tile)
            }
        }
        return tiles
    }
    
    override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void){
        ImageLoader.shared.loadImage(from: self.url(forTilePath: path)).sink {
            image in result(image?.jpegData(compressionQuality: 0.5), nil)
        }
    }
}

class DisneyTileRenderer: MKTileOverlayRenderer {
    
    public override func canDraw(_ mapRect: MKMapRect, zoomScale: MKZoomScale) -> Bool {
        let _ = super.canDraw(mapRect, zoomScale: zoomScale)
        return true
    }
    
    /// Draws the tile in the map
    /// - Parameters:
    ///     - mapRect: the map rect where the tiles need to be drawn
    ///     - zoomScale: current zoom in the map
    public override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
        
        // use default rendering if the type of overlay is not CachedTileOverlay
        guard let cachedOverlay = overlay as? DisneyMapOverlay else {
            super.draw(mapRect, zoomScale: zoomScale, in: context)
            return
        }
        
        //Extract the zoomable tiles for this mapRect
        let z = zoomScale.toZoomLevel(bounding_rect: cachedOverlay.boundingMapRect, draw_area: rect(for: mapRect))
        var zoom = (z >= 19) ? zoomScale * 2 : zoomScale;
        
        let tiles = cachedOverlay.TilesInMapRect(z: z, rect: mapRect, scale: zoom)
        
        for tile in tiles {
            cachedOverlay.loadTile(at: tile.maximumZPath) { [weak self] (data, error) in
                guard let strongSelf = self,
                      let data = data,
                      let provider = CGDataProvider(data: data as CFData),
                      let image = CGImage(jpegDataProviderSource: provider, decode: nil, shouldInterpolate: false, intent: CGColorRenderingIntent.defaultIntent)
                        ?? CGImage(pngDataProviderSource: provider, decode: nil, shouldInterpolate: false, intent: CGColorRenderingIntent.defaultIntent)
                else { return }
                
                let tileScaleFactor = CGFloat(tile.overZoom) / zoom
                let cgRect = strongSelf.rect(for: tile.rect)
                let drawRect = CGRect(x: 0, y: 0, width: CGFloat(image.width), height: CGFloat(image.height))
                context.saveGState()
                context.translateBy(x: cgRect.minX, y: cgRect.minY)
                context.scaleBy(x: tileScaleFactor, y: tileScaleFactor)
                if (z >= 19) {
                    context.translateBy(x: 0, y: CGFloat(image.height));
                } else {
                    context.translateBy(x: 0, y: CGFloat(image.height))
                }
                context.scaleBy(x: 1, y: -1)
                context.draw(image, in: drawRect)
                context.restoreGState()
            }
        }
    }
}

extension MKZoomScale {
    ///
    /// Converts from standard MapKit MKZoomScale to tile zoom level
    /// - Parameter tileSize: current map tile size in pixels. Typically set in MapCacheConfig
    /// - Returns: Corresponding zoom level for a tile
    func toZoomLevel(bounding_rect: MKMapRect, draw_area: CGRect) -> Int {
        let view_ratio = bounding_rect.maxX / Double(draw_area.width);
        return Int(log2(view_ratio) + 1);
    }
}
