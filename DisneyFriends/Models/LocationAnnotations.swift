//
//  LocationAnnotations.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 11/24/23.
//  Copyright © 2023 Rettify. All rights reserved.
//

import Foundation
import SwiftUI

class LocationAnnotations: ObservableObject {
    @Published var list: [LocationAnnotation]
    
    init(list: [LocationAnnotation]){
        self.list = list;
    }
}
