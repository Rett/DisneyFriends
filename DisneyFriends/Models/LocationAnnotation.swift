//
//  LocationAnnotation.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 12/26/19.
//  Copyright © 2019 Garrett. All rights reserved.
//

import SwiftUI
import UIKit
import MapKit

enum LocationType: String, CaseIterable, Identifiable {
    var id: Self { self }
    case ride;
    case dining;
      
    func Image() -> UIImage! {
        switch self {
        case .ride:
            return UIImage(systemName: "fireworks");
        case .dining:
            return UIImage(systemName: "fork.knife");
        }
    }
    
    func ToSystemPath() -> String {
        switch self {
        case .ride:
            return "fireworks";
        case .dining:
            return "fork.knife";
        }
    }
    

    static func FromString(type: String) -> LocationType {
        switch type.lowercased() {
        case "ride":
            return LocationType.ride;
        case "dining":
            return LocationType.dining;
        default:
            // TODO: Need a default case
            return LocationType.ride;
        }
    }
    
    static func ToString(type: LocationType) -> String {
        switch type {
        case .ride:
            return "Ride";
        case .dining:
            return "Dining";
        }
    }
}

class LocationAnnotation: MKPointAnnotation, Identifiable, ObservableObject {
    @Published var type: LocationType {
        didSet {
            typeChanged?()
            changed?()
        }
    }
    @Published var done: Bool {
        didSet {
            doneChanged?()
            changed?()
        }
    };
    @Published var information: String {
        didSet {
            self.subtitle = information
        }
    }
    @Published var event_name: String {
        didSet {
            self.title = event_name
        }
    }
    var destroyed: Bool = false;
    var uuid: UUID = UUID();
    var typeChanged: (()->())?
    var doneChanged: (()->())?
    var changed: (()->())?
    var shouldDelete: (()->())?

    init(uuid: UUID, coordinate: CLLocationCoordinate2D, title: String, type: LocationType, description: String, done: Bool) {
        self.type = type
        self.uuid = uuid;
        self.done = done;
        self.information = description
        self.event_name = title
        
        super.init();
        
        self.coordinate = coordinate;
        self.title = self.event_name;
        self.subtitle = self.information;
    }
}
