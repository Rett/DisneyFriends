//
//  LocationParser.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 12/26/19.
//  Copyright © 2019 Rettify. All rights reserved.
//

import Foundation

struct LocationCollection : Decodable {
    init() {
        let urlBar = Bundle.main.url(forResource: "Locations", withExtension: "geojson")!
        do {
            let jsonData = try Data(contentsOf: urlBar)
            let result = try JSONDecoder().decode(LocationCollection.self, from: jsonData)
            self.features = result.features;
            self.type = result.type;
            
            for feature in result.features {
                print("name", feature.properties.name, "name", feature.properties.name)
            }
        } catch {
            print("Error while parsing: \(error)")
            self.features = [];
            self.type = "NULL";
        }
    }
    
    let type : String
    let features : [Feature]
}

struct Feature : Decodable {
    let type : String
    let geometry : GeometryProperties
    let properties: Properties
}

struct GeometryProperties : Decodable {
    let type : String
    let coordinates : [Double]
}

struct Properties : Decodable {
    let name : String
}

func ParseLocations() {
    
}
