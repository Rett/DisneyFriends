//
//  ChecklistItem.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 11/21/23.
//  Copyright © 2023 Rettify. All rights reserved.
//

import SwiftUI
import MapKit

struct ChecklistItem: View {
    @StateObject var annotation: LocationAnnotation
    
    var body: some View {
        HStack {
            VStack {
                HStack {
                    Image(systemName: annotation.done ? "checkmark.circle.fill" : "circle").resizable().frame(width: 24, height: 24)
                        .foregroundColor(.green)
                    Text(annotation.title!).fontWeight(.medium)
                    Spacer()
                }
                Spacer().fixedSize()
                HStack {
                    if #available(iOS 17.0, *) {
                        Text(annotation.subtitle!).foregroundStyle(.gray)
                    } else {
                        Text(annotation.subtitle!).foregroundColor(.gray)
                    }
                    Spacer()
                }
            }
            Spacer()
        }.contentShape(Rectangle())
        .onTapGesture {
            annotation.done.toggle();
        }
    }
}

#Preview {
    ChecklistItem(annotation: LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 23.0), title: "Title", type: .ride, description: "DESCRIPTION!", done: false));
}
