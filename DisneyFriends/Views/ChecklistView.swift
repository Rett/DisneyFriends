//
//  ChecklistVIew.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 11/24/23.
//  Copyright © 2023 Rettify. All rights reserved.
//

import SwiftUI
import MapKit

struct ChecklistView: View {
    @ObservedObject var m_annotations: LocationAnnotations
    var annotationChanged: ((LocationAnnotation)->())?
    var body: some View {
        List {
            Section() {
                ForEach(m_annotations.list) { annotation in
                    ChecklistItem(annotation: annotation)
                }
                .onDelete(perform: delete)
            } header: {
                Text("Checklist")
                    .font(.system(size: 30, weight: .bold, design: .default))
                    .padding(EdgeInsets(top: 0, leading: -15, bottom: 0, trailing: 0))
            }
        }.padding(.vertical, 30)
    }
    
    func delete(at offsets: IndexSet) {
        for index in offsets {
            m_annotations.list[index].shouldDelete?();
            m_annotations.list[index].destroyed = true;
        }
        m_annotations.list.remove(atOffsets: offsets)
    }
}

#Preview {
    ChecklistView(m_annotations: LocationAnnotations(list: [
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title", type: .ride, description: "Description1 asdah osdha osdhoas dasholdla s", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title2", type: .ride, description: "Description2", done: true),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title3", type: .ride, description: "Description3", done: true),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
        LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 24.0), title: "Title4", type: .dining, description: "Description4", done: false),
    ]))
}
