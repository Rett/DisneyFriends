//
//  AnnotationEditView.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 11/24/23.
//  Copyright © 2023 Rettify. All rights reserved.
//

import SwiftUI
import MapKit

struct AnnotationEditView: View {
    @State var delete_confirmation: Bool = false
    @StateObject var annotation: LocationAnnotation
    
    init(annotation: LocationAnnotation) {
        self._annotation = StateObject(wrappedValue: annotation)
        UISegmentedControl.appearance().selectedSegmentTintColor = .systemBlue
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
    }
    
    var body: some View {
        VStack() {
            HStack {
                TextField("Event Title", text: $annotation.event_name)
                    .font(.system(size: 30, weight: .bold, design: .default))
                Spacer()
                Button(action: {annotation.done.toggle()}) {
                    if annotation.done {
                        Label("", systemImage: "checkmark.circle.fill").foregroundStyle(.green)
                    } else {
                        Label("", systemImage: "checkmark.circle")
                    }
                }.frame(width: 50, height: 50)
                Button(action: {self.delete_confirmation = true}) {
                    Label("", systemImage: "trash").foregroundStyle(.red)
                }
                .confirmationDialog("Delete this event?", isPresented: $delete_confirmation ) {
                    Button("Delete") {
                        withAnimation {
                            annotation.destroyed = true;
                            annotation.shouldDelete?();
                        }
                    }
                }
            }
            HStack(spacing: 0) {
                Picker("Type", selection: $annotation.type) {
                    ForEach(LocationType.allCases) { type in
                        Image(systemName: type.ToSystemPath())
                    }
                }.pickerStyle(.segmented)
            }.frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/).padding(EdgeInsets(top: 15, leading: 0, bottom: 0, trailing: 0))
            TextEditor(text: $annotation.information)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                .frame(maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
            Spacer()
        }.padding().padding(EdgeInsets(top: 15, leading: 0, bottom: 0, trailing: 0))
    }
}

#Preview {
    AnnotationEditView(annotation: LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: 23.0, longitude: 23.0), title: "Title", type: .ride, description: "DESCRIPTION osdfhiosdfh ioshfhosdfhohsuofhoshfisldhf hshfkshdfsk hfkhske hfkas dasdkasd askshef kshfkhsekfhs f!", done: false));
}
