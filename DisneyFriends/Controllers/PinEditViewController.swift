//
//  PinEditViewController.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 11/20/23.
//  Copyright © 2023 Rettify. All rights reserved.
//

import UIKit
import MapKit
import SwiftUI

class PinEditViewController: UIViewController, UITextViewDelegate {
    @IBOutlet var m_view: UIView!
    var m_swift_view: UIView!;
    var m_type: LocationType = .ride;
    var m_annotation: LocationAnnotation!
    
    override func viewDidLoad() {
        super.viewDidLoad();
    }
    
    func SetAnnotation(annotation: LocationAnnotation) {
        m_annotation = annotation;
        
        if (m_swift_view != nil) {
            m_swift_view.removeFromSuperview();
        }
        
        let edit_view = AnnotationEditView(annotation: self.m_annotation);
        let vc = UIHostingController(rootView: edit_view);
        m_swift_view = vc.view!
        m_swift_view.translatesAutoresizingMaskIntoConstraints = false
        addChild(vc)
        
        m_view.addSubview(m_swift_view);
        let constraints = [
            vc.view.leadingAnchor.constraint(equalTo: m_view.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: m_view.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: m_view.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: m_view.bottomAnchor),
            vc.view.widthAnchor.constraint(equalTo: m_view.widthAnchor)
        ]
        m_view.addConstraints(constraints)
    }
    
}
