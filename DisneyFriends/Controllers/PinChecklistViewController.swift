//
//  PinChecklistController.swift
//  DisneyFriends
//
//  Created by Garrett Phelps on 11/20/23.
//  Copyright © 2023 Rettify. All rights reserved.
//

import UIKit
import MapKit
import SwiftUI
import FloatingPanel

class PinChecklistViewController: UIViewController {
    @IBOutlet var m_view: UIView!
    var m_swift_view: UIView!;
    @ObservedObject var m_annotations: LocationAnnotations = LocationAnnotations(list: [LocationAnnotation]())
    
    override func viewDidLoad() {
        super.viewDidLoad();
    }
    
    func SetAnnotations(annotations: LocationAnnotations){
        m_annotations = annotations
        
        if (m_swift_view != nil) {
            m_swift_view.removeFromSuperview();
        }
        
        let checklist_view = ChecklistView(m_annotations: m_annotations);
        let vc = UIHostingController(rootView: checklist_view);
        m_swift_view = vc.view!
        m_swift_view.translatesAutoresizingMaskIntoConstraints = false
        addChild(vc)
        
        m_view.addSubview(m_swift_view);
        let constraints = [
            m_swift_view.leadingAnchor.constraint(equalTo: m_view.leadingAnchor),
            m_swift_view.trailingAnchor.constraint(equalTo: m_view.trailingAnchor),
            m_swift_view.topAnchor.constraint(equalTo: m_view.topAnchor),
            m_swift_view.bottomAnchor.constraint(equalTo: m_view.bottomAnchor),
            m_swift_view.widthAnchor.constraint(equalTo: m_view.widthAnchor)
        ]
        m_view.addConstraints(constraints)
    }
}

final class SearchPanelPhoneDelegate: FloatingPanelControllerDelegate {
    func floatingPanelWillBeginDragging(_ vc: FloatingPanelController) {
        if vc.state == .full {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        }
    }
}
