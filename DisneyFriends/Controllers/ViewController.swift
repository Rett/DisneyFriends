//
//  ViewController.swift
//  DisneyFriends
//
//  Created by Garrett on 8/12/18.
//  Copyright © 2018 Garrett. All rights reserved.
//

import UIKit
import MapKit
import MessageUI
import SwiftUI
import FloatingPanel
import MapCache

class ViewController: UIViewController, MFMessageComposeViewControllerDelegate, FloatingPanelControllerDelegate, UNUserNotificationCenterDelegate
{
    @IBOutlet weak var m_button_location: UIButton!
    @IBOutlet weak var m_button_add_pin: UIButton!
    @IBOutlet weak var m_button_checklist: UIButton!
    @IBOutlet weak var m_button_kingdom: UIButton!
    @IBOutlet weak var m_map_view: MKMapView!
    @IBOutlet weak var m_pin_view: UIView!
    let m_location_manager = CLLocationManager();
    var m_tile_renderer: DisneyTileRenderer!;
    var m_map_center_init = false;
    var m_last_location: CLLocation = CLLocation();
    var m_app_delegate = UIApplication.shared.delegate as! AppDelegate;
    var m_pin_edit_fpc: FloatingPanelController = FloatingPanelController();
    var m_pin_edit_controller: PinEditViewController?;
    var m_pin_checklist_fpc: FloatingPanelController = FloatingPanelController();
    var m_pin_checklist_controller: PinChecklistViewController?;
    var m_active_annotation: LocationAnnotation = LocationAnnotation(uuid: UUID(), coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(), longitude: CLLocationDegrees()), title: "", type: .ride, description: "", done: false);
    @ObservedObject var m_visible_annotations: LocationAnnotations = LocationAnnotations(list: [LocationAnnotation]())
    var m_location_collection: LocationCollection = LocationCollection();
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        if traitCollection.userInterfaceStyle == .light
        {
            return .darkContent;
        }
        else
        {
            return .darkContent;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        SetupTileRenderer();
        
        self.m_location_manager.requestAlwaysAuthorization();
        self.m_location_manager.requestWhenInUseAuthorization();
        
        if CLLocationManager.locationServicesEnabled()
        {
            m_location_manager.delegate = self;
            m_location_manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            m_location_manager.startUpdatingLocation();
        }
        
        m_map_view.delegate = self;
        m_map_view.showsUserLocation = true;
        
        m_pin_edit_fpc.delegate = self;
        guard let pin_edit_controller = storyboard?.instantiateViewController(withIdentifier: "pin_edit_view") as? PinEditViewController else {
            return
        }
        m_pin_edit_controller = pin_edit_controller;
        m_pin_edit_fpc.set(contentViewController: m_pin_edit_controller);
        
        m_pin_checklist_fpc.delegate = self;
        guard let pin_checklist_controller = storyboard?.instantiateViewController(withIdentifier: "pin_checklist_view") as? PinChecklistViewController else {
            return
        }
        m_pin_checklist_controller = pin_checklist_controller;
        m_pin_checklist_fpc.set(contentViewController: m_pin_checklist_controller);
        
        m_button_location.layer.cornerRadius = 7.0;
        m_button_location.layer.shadowColor = UIColor.black.cgColor
        m_button_location.layer.shadowOffset = .zero
        m_button_location.layer.shadowRadius = 7.0
        m_button_location.layer.shadowOpacity = 0.5
        m_button_location.layer.shadowPath = UIBezierPath(rect: m_button_location.bounds).cgPath;
        
        m_button_add_pin.layer.cornerRadius = 7.0;
        m_button_add_pin.layer.shadowColor = UIColor.black.cgColor
        m_button_add_pin.layer.shadowOffset = .zero
        m_button_add_pin.layer.shadowRadius = 7.0
        m_button_add_pin.layer.shadowOpacity = 0.5
        m_button_add_pin.layer.shadowPath = UIBezierPath(rect: m_button_add_pin.bounds).cgPath;
        
        m_button_checklist.layer.cornerRadius = 7.0;
        m_button_checklist.layer.shadowColor = UIColor.black.cgColor
        m_button_checklist.layer.shadowOffset = .zero
        m_button_checklist.layer.shadowRadius = 7.0
        m_button_checklist.layer.shadowOpacity = 0.5
        m_button_checklist.layer.shadowPath = UIBezierPath(rect: m_button_checklist.bounds).cgPath;
        
        m_button_kingdom.layer.cornerRadius = 7.0;
        m_button_kingdom.layer.shadowColor = UIColor.black.cgColor
        m_button_kingdom.layer.shadowOffset = .zero
        m_button_kingdom.layer.shadowRadius = 7.0
        m_button_kingdom.layer.shadowOpacity = 0.5
        m_button_kingdom.layer.shadowPath = UIBezierPath(rect: m_button_kingdom.bounds).cgPath;
        
        
        let tapMap = UITapGestureRecognizer(target: self, action:(#selector(OnMapTapped)))
        m_map_view.addGestureRecognizer(tapMap);
        
        m_map_view.showsCompass = false
        let compassBtn = MKCompassButton(mapView: m_map_view)
        compassBtn.frame.origin = CGPoint( x: 15, y: 80 );
        compassBtn.compassVisibility = .adaptive
        view.addSubview(compassBtn)
        
        m_map_view.mapType = .satellite;
        
        m_location_collection = LocationCollection();
        m_location_manager.allowsBackgroundLocationUpdates = true;
        SetupNotifications();
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let region_radius: CLLocationDistance = 1000;
    func CenterMap(location: CLLocation)
    {
        let coordinate_region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: region_radius, longitudinalMeters: region_radius);
        m_map_view.setRegion(coordinate_region, animated: true);
    }
    
    func SetupTileRenderer()
    {
        let overlay = DisneyMapOverlay();
        overlay.canReplaceMapContent = true;
        overlay.minimumZ = 11;
        overlay.maximumZ = 21;
        overlay.tileSize = CGSize(width: 256, height: 256);

        m_map_view.addOverlay(overlay, level: .aboveRoads);
        m_tile_renderer = DisneyTileRenderer(tileOverlay: overlay);
    }
    
    func AddLocationPins() {
        guard m_location_collection.features.count > 0 else { return; }
        
        for location in m_location_collection.features {
            let latitude = location.geometry.coordinates[0];
            let longitude = location.geometry.coordinates[1];
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude);
            let title = location.properties.title;
            let description = location.properties.description;
            let type = LocationType.FromString(type: location.type);
            let uuid = UUID(uuidString: location.uuid);
            let done = location.properties.done;
            let annotation = LocationAnnotation(uuid: uuid!, coordinate: coordinate, title: title, type: type, description: description, done: done);
            m_map_view.addAnnotation(annotation);
      }
    }
    
    func DetectClosePins() {
        for i in m_map_view.annotations.indices {
            let annotation = m_map_view.annotations[i];
            guard let location_annotation = annotation as? LocationAnnotation else { continue }
            
            let annotation_lat = location_annotation.coordinate.latitude;
            let annotation_lon = location_annotation.coordinate.longitude;
            let distance = m_last_location.distance(from: CLLocation(latitude: annotation_lat, longitude: annotation_lon));
            if (distance < 30 && !location_annotation.done) {
                let content = UNMutableNotificationContent()
                content.title = "\(location_annotation.title!)"
                content.body = "You have arrived! Would you like to complete this event?"
                content.userInfo = ["UUID" : location_annotation.uuid.uuidString]
                content.categoryIdentifier = "EVENT_ARRIVAL"
                content.sound = UNNotificationSound.default
                let request = UNNotificationRequest(identifier: "EVENT_ARRIVAL", content: content, trigger: nil);
                
                let notification_center = UNUserNotificationCenter.current()
                notification_center.add(request);
            }
        }
    }
    
    func UpdateVisibleAnnotations(annotations: LocationAnnotations) {
        m_visible_annotations.list = m_visible_annotations.list.filter({ annotations.list.contains($0) })
        for annotation in annotations.list {
            if (!m_visible_annotations.list.contains(annotation)) {
                m_visible_annotations.list.append(annotation)
                annotation.changed = {
                    self.m_location_collection.Update(annotation: annotation);
                }
                annotation.shouldDelete = {
                    self.m_visible_annotations.list.removeAll(where: { $0.uuid == annotation.uuid });
                    self.m_location_collection.Remove(annotation: annotation);
                    self.m_map_view.removeAnnotation(annotation);
                }
            }
        }
    }
    
    func AddVisibleAnnotation(annotation: LocationAnnotation) {
        if (!m_visible_annotations.list.contains(annotation)) {
            m_visible_annotations.list.append(annotation)
            annotation.changed = {
                self.m_location_collection.Update(annotation: annotation);
            }
            annotation.shouldDelete = {
                self.m_visible_annotations.list.removeAll(where: { $0.uuid == annotation.uuid });
                self.m_location_collection.Remove(annotation: annotation);
                self.m_map_view.removeAnnotation(annotation);
            }
        }
    }

    
    func SetupNotifications() {
        let complete_action = UNNotificationAction(identifier: "COMPLETE_ACTION",
              title: "Complete",
              options: [])
        let cancel_action = UNNotificationAction(identifier: "CANCEL_ACTION",
              title: "Cancel",
              options: [])
        // Define the notification type
        let arrived_notification_category =
              UNNotificationCategory(identifier: "EVENT_ARRIVAL",
              actions: [complete_action, cancel_action],
              intentIdentifiers: [],
              hiddenPreviewsBodyPlaceholder: "",
              options: .customDismissAction)
        // Register the notification type.
        let notification_center = UNUserNotificationCenter.current()
        notification_center.setNotificationCategories([arrived_notification_category])
        
        notification_center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                UNUserNotificationCenter.current().delegate = self
            } else {
            }
        }
    }
    
    @objc func OnMapTapped() {
        m_pin_edit_fpc.dismiss(animated: true);
        m_pin_checklist_fpc.dismiss(animated: true);
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    @IBAction func InviteToMap(_ sender: Any)
    {
    }
    
    @IBAction func ButtonMyLocationClicked(_ sender: Any)
    {
        switch m_map_view.userTrackingMode
        {
        case .none:
            m_map_view.userTrackingMode = .follow;
            break;
        case .follow:
            m_map_view.userTrackingMode = .followWithHeading;
            break;
        case .followWithHeading:
            m_map_view.userTrackingMode = .none;
            break;
        default:
            break;
        }
    }
    
    @IBAction func ButtonAddPinClicked(_ sender: Any)
    {
        let coordinate = m_map_view.centerCoordinate;
        let title = "Pin"
        let annotation = LocationAnnotation(uuid: UUID(), coordinate: coordinate, title: title, type: .ride, description: "Description", done: false);
        m_map_view.addAnnotation(annotation);
        m_map_view.selectAnnotation(annotation, animated: true);
    }
    
    @IBAction func ButtonChecklistClicked(_ sender: Any)
    {
        if (m_pin_checklist_fpc.parent != nil) {
            m_pin_checklist_fpc.dismiss(animated: true);
        } else {
            m_pin_checklist_controller?.SetAnnotations(annotations: m_visible_annotations)
            m_pin_checklist_fpc.addPanel(toParent: self, animated: true);
        }
    }
    
    @IBAction func ButtonKingdomClicked(_ sender: Any)
    {
        m_map_view.centerCoordinate = CLLocationCoordinate2D(latitude: 28.420328, longitude: -81.581910)
    }
    
    @IBAction func CreateMap(_ sender: Any)
    {
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
           didReceive response: UNNotificationResponse,
           withCompletionHandler completionHandler:
             @escaping () -> Void) {
           
       // Get the meeting ID from the original notification.
        let userInfo = response.notification.request.content.userInfo
        let uuid_string = userInfo["UUID"] as! String;
        let uuid = UUID(uuidString: uuid_string);
        
        var found_annotation: LocationAnnotation? = nil;
        for i in m_map_view.annotations.indices {
            let annotation = m_map_view.annotations[i];
            guard let location_annotation = annotation as? LocationAnnotation else { continue }
            
            if location_annotation.uuid == uuid {
                found_annotation = location_annotation;
                break;
            }
        }
        
        // Perform the task associated with the action.
        switch response.actionIdentifier {
        case "COMPLETE_ACTION":
           found_annotation?.done = true;
          break
            
        case "CANCEL_ACTION":
          break

        default:
          break
        }
        
        // Always call the completion handler when done.
        completionHandler()
    }
}

extension ViewController: MKMapViewDelegate
{
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return m_tile_renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is LocationAnnotation || annotation is MKClusterAnnotation
        {
            let annotationView = LocationAnnotationsView(annotation: annotation, reuseIdentifier: "Location")
            if (annotation is LocationAnnotation) {
                let location_annotation = annotation as! LocationAnnotation;
                AddVisibleAnnotation(annotation: location_annotation)
                annotationView.isDraggable = true
            } else {
                annotationView.isDraggable = false
            }

            return annotationView
        }
        else
        {
            return mapView.view(for: annotation);
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let annotations_set = m_map_view.annotations(in: m_map_view.visibleMapRect);
        let annotations = Array(annotations_set);
        let visible_annotations = LocationAnnotations(list: [LocationAnnotation]());
        for annotation in annotations {
            guard let location_annotation = annotation as? LocationAnnotation else { continue }
            visible_annotations.list.append(location_annotation)
        }
        
        UpdateVisibleAnnotations(annotations: visible_annotations)
        m_pin_checklist_controller?.SetAnnotations(annotations: m_visible_annotations);
    }
        
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation as? LocationAnnotation {
            m_active_annotation = annotation;
            m_pin_edit_controller?.SetAnnotation(annotation: m_active_annotation);
            m_pin_edit_fpc.addPanel(toParent: self, animated: true);
            m_pin_checklist_fpc.dismiss(animated: true);
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if let annotation = view.annotation as? LocationAnnotation {
            if !annotation.destroyed {
                m_location_collection.Update(annotation: annotation);
            }
        }
        m_pin_edit_fpc.dismiss(animated: true);
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        if (mapView.visibleMapRect.height < 512)
        {
            mapView.visibleMapRect = MKMapRect(x: mapView.visibleMapRect.origin.x, y: mapView.visibleMapRect.origin.y, width: mapView.visibleMapRect.width, height: 512)
        }
    }
    
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool)
    {
        var image_name = "location";
        switch m_map_view.userTrackingMode
        {
        case .none:
            image_name = "location";
            break;
        case .follow:
            image_name = "location.fill";
            break;
        case .followWithHeading:
            image_name = "location.north.line.fill";
            break;
        default:
            break;
        }
        m_button_location.setImage(UIImage(systemName: image_name), for: UIControl.State.normal);
    }
}

extension ViewController: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let lat = locations.last?.coordinate.latitude, let lon = locations.last?.coordinate.longitude
        {
            m_last_location = CLLocation(latitude: lat, longitude: lon);
            if (!m_map_center_init)
            {
                CenterMap(location: CLLocation(latitude: lat, longitude: lon));
                print("\(lat), \(lon)");
                m_map_center_init = true;
                AddLocationPins();
            }
            
            DetectClosePins();
        }
        else
        {
            print("NO COORDINATES!");
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print(error);
    }
    
}
